
import GLOBAL_ENV from '../../data/global_env'

import { Component } from '@angular/core';
import { Nav,NavController, ModalController, Platform,
         PopoverController } from 'ionic-angular';
import { LanguageOptionsPage } from '../language-options/language-options'
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';
import { WelcomeTutorialPage } from '../welcome-tutorial/welcome-tutorial';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  _storage: Storage;
  platform : Platform;
  // isMobileFree: boolean; //= GLOBAL_ENV[0].TMOJI_MOBILE_FREE;
  deviceLanguage: any = window.navigator.language.split('-')[0];

  tmojiClicked: boolean = false;

  constructor(
    public nav: Nav,
    public navCtrl: NavController,
    public modalCtrl : ModalController,
    public popoverCtrl: PopoverController,
    public socialSharing: SocialSharing,
    private storage: Storage) {

      this._storage = storage;

      storage.get('userLangSelected').then((userLangSelected) => {
        if(userLangSelected == undefined) return
          this.deviceLanguage = userLangSelected;
          console.log("userLangSelected",userLangSelected);
        
      });

      // document.getElementById("tmojiLogo")
      // .classList.remove('animated','tada');

  
  }


  onShowWelcomeSlider(){
      this.nav.setRoot(WelcomeTutorialPage, {}, {
        animate: true,
        direction: 'back',
        animation: 'ios-transition'
      });
  }

  tmojiClick(){
    // if(!this.tmojiClicked){
        document.getElementById("tmojiLogo")
                  .classList.add('animated','tada');
    // } else {
      setTimeout( () => { 
        document.getElementById("tmojiLogo")
      .classList.remove('animated','tada');
      }, 1500 )
        
                  
    // }
    // this.tmojiClicked = !this.tmojiClicked;
  }

  async sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  swipeEvent(e) {
     if(e.direction == 2){
          this.navCtrl.parent.select(6);
     } else if (e.direction == 4){
          this.navCtrl.parent.select(4);
     }
  }

  // language options
  showLangOptions() {
    const modal =
      this.popoverCtrl.create(
        LanguageOptionsPage, {},
        { cssClass: "custom-popover" });
    modal.present();
    modal.onDidDismiss((langObject: any) => {
    
      if (!langObject) { return };
      if (langObject.action == 'save'){
        this.storage.set('localLangSelectedLabel', langObject.localLang);
        this.storage.set('userLangSelectedLabel', langObject.userLang);
        this.storage.set('localLangSelected', langObject.localLangCode);
        this.storage.set('userLangSelected', langObject.userLangCode);

        this.deviceLanguage = langObject.userLangCode;

      }
      

    });

   
  }

  ionViewWillEnter() {
    this._storage.get('userLangSelected').then((userLangSelected) => {
      if(userLangSelected == undefined) return 
        this.deviceLanguage = userLangSelected;
        console.log("userLangSelected",userLangSelected);
      
    });

      document.getElementById("tmojiLogo")
        .classList.remove('animated','tada');
  }

  onTellFriend(){
    this._storage.get('userLangSelected').then((userLangSelected) => {
      if(this.deviceLanguage != ''){
        this.deviceLanguage = userLangSelected;
        console.log("userLangSelected",userLangSelected);
      }
    });
    
    // if (this.platform.is('android') || this.platform.is('ios') ) {
    //       if(GLOBAL_ENV[0].TMOJI_MOBILE_FREE){
    //           this.isMobileFree = true;
    //           console.log(this.isMobileFree)
    //       }
    // } else {
    //       console.log(this.isMobileFree)
    // }

    let shareMessage;
    if (this.deviceLanguage == 'en' ){
  	  shareMessage = 'Hey check this pretty cool app, Tmoji (translate with emojis) http://tmojiapp.com';
  	} else if (this.deviceLanguage == 'es'){
  		shareMessage = 'Mira esta aplicación genial, Tmoji (traduce con emojis) http://tmojiapp.com';
  	} else if (this.deviceLanguage == 'fr'){
  		shareMessage = 'Hey vérifier cette jolie application génial, Tmoji (traduire avec emojis) http://tmojiapp.com';
  	} else if (this.deviceLanguage == 'ru'){
  		shareMessage = 'Эй, проверьте это довольно крутое приложение, Tmoji (переведите с emojis) http://tmojiapp.com';
  	} else if (this.deviceLanguage == 'ja'){
  		shareMessage = 'ねえ、これはかなりクールなアプリ、Tmoji（emojisと翻訳する）を確認する http://tmojiapp.com';
  	} else if (this.deviceLanguage == 'zh'){
  		shareMessage = '嘿检查这个非常酷的应用程序，Tmoji（用表情符号翻译）http://tmojiapp.com';
  	} else if (this.deviceLanguage == 'hi'){
  		shareMessage = 'हे इस सुंदर कूल ऐप की जांच करें, Tmoji (इमोजिस के साथ अनुवाद) http://tmojiapp.com';
  	} else if (this.deviceLanguage == 'de'){
  		shareMessage = 'Hey, sieh dir diese ziemlich coole App an, Tmoji (Übersetzung mit Emojis) http://tmojiapp.com';
  	} else if (this.deviceLanguage == 'pt'){
  		shareMessage = 'Ei, verifique este aplicativo muito legal, Tmoji (traduzir com emojis) http://tmojiapp.com';
  	} else if (this.deviceLanguage == 'ar'){
  		shareMessage = 'Hey check out this wonderful app, Tmoji (translate with emoticons) http://tmojiapp.com';
  	} else {
      shareMessage = 'Hey check this pretty cool app, Tmoji (translate with emojis) http://tmojiapp.com';
    }


    // message, subject, file, url
    this.socialSharing.share(shareMessage).then(() => {
      // Sharing via email is possible

    }).catch(() => {
      // Sharing via email is not possible
    });

    // this.socialSharing.shareViaEmail('Body', 'Subject', ['recipient@example.org']).then(() => {
    // // Success!
    // }).catch(() => {
    //   // Error!
    // });

  }

  onRemoveAds(){
    
  }

  onSendFeedback(){  
    // add omespino+tmojifeedback@gmail.com if won't work
    let mailtoEng = "mailto:omespino+tmojifeedback@gmail.com?"+
                    "subject=Tmoji App feedback" + 
                    "&body=Hello Tmoji team, I have found ..." + 
                    "%0A%0d%0A%0d%0A%0d%0A%0d" + // %0a0d = 1 break line
                    "Regards"

    window.location.href = mailtoEng;

  }
  


}
