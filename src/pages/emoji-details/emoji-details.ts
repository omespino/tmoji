import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import country_codes from '../../data/country_codes';
import country_srvs from '../../data/country_srvs';
import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

 @Component({
  selector: 'page-emoji-details',
  templateUrl: 'emoji-details.html',
})

 export class EmojiDetailsPage {

  // nav : string;
  // navParams : NavParams;
  emojiDetail: any = [];

  langSelectedByUser: any = [];
  countryCode: string;

  locationLanguageSelected: string ='';
  userLanguageSelected: string ='';

  // device lang
  locationLang : any;// = 'en'
  deviceLang : any = window.navigator.language.split('-')[0];

  constructor(
        private navParams: NavParams,
        private httpClient: HttpClient,
        private events : Events,
        private storage: Storage
  ) {

    // manual mode
    if(this.deviceLang == 'en' ) {
      this.locationLang = 'es'
      // storage.set('localLangSelected', 'es')
    } else {
      this.locationLang = 'en'
      // storage.set('localLangSelected', 'en')
    }

    storage.get('localLangSelected').then((localLangSelected) => {
      if(localLangSelected == undefined) return
      console.log('entro? localLangSelected', localLangSelected);
      this.locationLang = localLangSelected;
    });
    

    storage.get('userLangSelected').then((userLangSelected) => {
      if(userLangSelected == undefined) return
      console.log('entro userLangSelected?', userLangSelected);
      this.deviceLang = userLangSelected;
    });

    this.navParams = navParams;

  }

  getLangByCountryCode(CODE: string){
      
      let lang = "";

      for (var country of country_codes) {
         if(country.code.includes(CODE))
             lang = country.lang;
      }

      return lang; 
  }

}
