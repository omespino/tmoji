import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmojiDetailsPage } from './emoji-details';

@NgModule({
  declarations: [
    EmojiDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(EmojiDetailsPage),
  ],
})
export class EmojiDetailsPageModule {}
