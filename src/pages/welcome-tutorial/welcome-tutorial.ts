import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, MenuController, Tab} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { Storage } from '@ionic/storage';

export interface Slide {
  title: string;
  description: string;
  image: string;
}

@Component({
  selector: 'page-welcome-tutorial',
  templateUrl: 'welcome-tutorial.html',
})
export class WelcomeTutorialPage {

  showSkip = true;
  slides: Slide[];
  tabsPage:any = TabsPage;
  
  slide1Title: string;
  slide1Description: string;
  slide2Title: string;
  slide2Description: string;

  slide3Title: string;
  slide3Description: string;

  slide4Title: string;
  slide4Description: string;

  lastDescription: string;
  lastButtonLabel: string;
  lastTitle: string;
  isLastSlide: boolean = false;
  skipLabel: string;

  deviceLanguage: any = window.navigator.language.split('-')[0];

  userLangSelected: string;

  constructor(
    public storage :Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    public menu: MenuController,
    ){
        this.storage.get('userLangSelected').then((userLangSelected) => {
          if(userLangSelected){
            this.userLangSelected = userLangSelected;
            console.log("userLangSelected slider",userLangSelected);
          }
        });

      //this.setTranslation();
  }

  startApp() {
    
    this.navCtrl.setRoot(TabsPage, {}, {
      animate: true,
      direction: 'forward',
      animation: 'ios-transition'
    });
    
    this.storage.set('showSlide',false);

  }

  setTranslation(){

    console.log("lang slider",this.userLangSelected)
    
    if (this.userLangSelected){
        this.deviceLanguage = this.userLangSelected
    } else {
       this.deviceLanguage = window.navigator.language.split('-')[0];
    }

    if (this.deviceLanguage == 'en' ){

      this.skipLabel = 'Skip';
      this.slide1Title = 'Hello welcome to Tmoji';
      this.slide1Description = 'Translate with emojis';

      this.slide2Title = 'Automatic translation';
      this.slide2Description = 
      'Tmoji take the place where you are and your language ' +
      'to translate hundreds of words instantly with emojis';

      this.slide3Title = 'Languages';
      this.slide3Description = 
      'Tmoji has the 10 most popular languages around the world';
 
      this.slide4Title = 'Categories';
      this.slide4Description = 
      'Tmoji has categories like: ' +
      'food, travel and places, emergencies and nature';

      this.lastTitle = "It's that easy!" ;
      this.lastDescription = 'The Tmoji team wish you ' +
                             'have a good trip!';
      this.lastButtonLabel = 'Start using Tmoji';

    } else if (this.deviceLanguage == 'es'){

      this.skipLabel = 'Omitir';
      this.slide1Title = 'Hola bienvenido a Tmoji';
      this.slide1Description = 'Traduce con emojis';

      this.slide2Title = 'Traducción automática';
      this.slide2Description = 
      'Tmoji toma el lugar donde estás y tu lenguaje ' +
      'para traducir cientos de palabras al instante con emojis';

      this.slide3Title = 'Idiomas';
      this.slide3Description = 
      'Tmoji cuenta con los 10 idiomas más poulares alrededor del mundo'; 
 
      this.slide4Title = 'Categorías';
      this.slide4Description = 
      'Tmoji tiene categorías como: ' +
      'comida, viajes y transporte, emergencias y naturaleza';

      this.lastTitle = '¡Así de fácil!';
      this.lastDescription = '¡El equipo de Tmoji te desea '+
                             'un buen viaje!';
      this.lastButtonLabel = 'Comenzar a usar Tmoji';

    } else if (this.deviceLanguage == 'fr'){

      this.skipLabel = 'Sauter';
      this.slide1Title = 'Bonjour bienvenue à Tmoji';
      this.slide1Description = 'Traduire avec emojis';

      this.slide2Title = 'Traduction automatique';
      this.slide2Description = 
      'Tmoji prend la place où tu es et ta langue ' +
      'pour traduire des centaines de mots instantanément avec des emojis';

      this.slide3Title = 'Langues';
      this.slide3Description = 
      'Tmoji a les 10 langues les plus populaires à travers le monde';
 
      this.slide4Title = 'Catégories';
      this.slide4Description = 
      'Tmoji a des catégories comme: ' +
      'nourriture, voyages et lieux, urgences et nature';

      this.lastTitle = "C'est si facile!" ;
      this.lastDescription = "L'équipe de Tmoji vous souhaite " +
                             'un bon voyage!';
      this.lastButtonLabel = 'Commencez à utiliser Tmoji';

    } else if (this.deviceLanguage == 'ru'){

      this.skipLabel = 'Пропускать';
      this.slide1Title = 'Приветствую вас в Tmoji';
      this.slide1Description = 'Перевести с эмозисом';

      this.slide2Title = 'Автоматический перевод';
      this.slide2Description = 
      'Tmoji займет место, где вы и ваш язык ' +
      'чтобы перевести сотни слов мгновенно с эмозисом';

      this.slide3Title = 'Языки';
      this.slide3Description = 
      'Tmoji имеет 10 самых популярных языков во всем мире';
 
      this.slide4Title = 'категории';
      this.slide4Description = 
      'Tmoji имеет такие категории, как: ' +
      'еда, путешествия и места, чрезвычайные ситуации и природа';

      this.lastTitle = "Это так просто!" ;
      this.lastDescription = "Команда Tmoji желает, чтобы у вас была хорошая поездка!";
      this.lastButtonLabel = 'Начните использовать Tmoji';

    } else if (this.deviceLanguage == 'ja'){

      this.skipLabel = 'スキップ';
      this.slide1Title = 'こんにちはTmojiへようこそ';
      this.slide1Description = 'emojisと翻訳してください';

      this.slide2Title = '自動翻訳';
      this.slide2Description = 
      'Tmojiは、あなたがいる場所とあなたの言語を使って、emojisですぐに何百もの言葉を翻訳します';

      this.slide3Title = '言語';
      this.slide3Description = 
      'Tmoji には世界で最も人気のある10の言語があります';
 
      this.slide4Title = 'カテゴリー';
      this.slide4Description = 
      'Tmojiには、食べ物、旅行や場所、緊急事態、自然などのカテゴリがあります';

      this.lastTitle = "それは簡単です！" ;
      this.lastDescription = "Tmojiチームはあなたに良い旅をしてほしい！";
      this.lastButtonLabel = 'Tmojiを使い始める';


    } else if (this.deviceLanguage == 'zh'){

      this.skipLabel = '跳跃';
      this.slide1Title = '您好欢迎来到Tmoji';
      this.slide1Description = '用表情符号翻译';

      this.slide2Title = '自动翻译';
      this.slide2Description = 
      'Tmoji 带着你的位置和你的语言用emojis立即翻译数百个单词';

      this.slide3Title = '语言';
      this.slide3Description = 
      'Tmoji拥有世界上最流行的10种语言';
 
      this.slide4Title = '分类';
      this.slide4Description = 
      'Tmoji有类别：食物，旅行和地方，紧急情况和自然';

      this.lastTitle = "就这么简单！" ;
      this.lastDescription = 'Tmoji团队祝您旅途愉快！';
      this.lastButtonLabel = '开始使用Tmoji';

    } else if (this.deviceLanguage == 'hi'){

      this.skipLabel = 'छोड़ें';
      this.slide1Title = 'हैलो आपका स्वागत है Tmoji';
      this.slide1Description = 'Emojis के साथ अनुवाद करें';

      this.slide2Title = 'स्वचालित अनुवाद';
      this.slide2Description = 
      'Tmoji उस जगह को ले जाएं जहां आप हैं और आपकी भाषा इमोजी के साथ तुरंत सैकड़ों शब्दों का अनुवाद करे';

      this.slide3Title = 'बोली';
      this.slide3Description = 
      'टॉमोजी दुनिया भर में 10 सबसे लोकप्रिय भाषाएं हैं';
 
      this.slide4Title = 'श्रेणियाँ';
      this.slide4Description = 
      'टीएमजीजी की श्रेणियां हैं: भोजन, यात्रा और स्थान, आपात स्थिति और प्रकृति';

      this.lastTitle = "इट्स दैट ईजी!" ;
      this.lastDescription = 'Tmoji टीम की इच्छा है कि आपकी अच्छी यात्रा है!';
      this.lastButtonLabel = 'Tmoji का उपयोग शुरू करें';

    } else if (this.deviceLanguage == 'de'){

      this.skipLabel = 'Überspringen';
      this.slide1Title = 'Hallo Willkommen bei Tmoji';
      this.slide1Description = 'Übersetze mit Emojis';

      this.slide2Title = 'Automatische Übersetzung';
      this.slide2Description = 
      'Tmoji nehmen Sie den Ort, wo Sie sind und Ihre Sprache, um Hunderte von Wörtern sofort mit Emojis zu übersetzen';

      this.slide3Title = 'Sprachen';
      this.slide3Description = 
      'Tmoji hat die 10 beliebtesten Sprachen der Welt';
 
      this.slide4Title = 'Kategorien';
      this.slide4Description = 
      'Tmoji hat Kategorien wie: Essen, Reisen und Orte, Notfälle und Natur';

      this.lastTitle = "So einfach ist das!" ;
      this.lastDescription = 'Das Tmoji-Team wünscht Ihnen eine gute Reise!';
      this.lastButtonLabel = 'Beginnen Sie mit Tmoji';

    } else if (this.deviceLanguage == 'pt'){

      this.skipLabel = 'Pular';
      this.slide1Title = 'Olá, bem vindo ao Tmoji';
      this.slide1Description = 'Traduzir com emojis';

      this.slide2Title = 'Tradução automática';
      this.slide2Description = 
      'Tmoji tomar o lugar onde você está e sua língua para traduzir centenas de palavras instantaneamente com emojis';

      this.slide3Title = 'Línguas';
      this.slide3Description = 
      'Tmoji tem as 10 línguas mais populares do mundo';
 
      this.slide4Title = 'Categorias';
      this.slide4Description = 
      'Tmoji tem categorias como: comida, viagens e lugares, emergências e natureza';

      this.lastTitle = "É tão fácil!" ;
      this.lastDescription = 'A equipe de Tmoji deseja que você tenha uma boa viagem!';
      this.lastButtonLabel = 'Comece a usar o Tmoji';

    } else if (this.deviceLanguage == 'ar'){

      this.skipLabel = 'تخطى';
      this.slide1Title = 'مرحبًا بكم في Tmoji'
      this.slide1Description = 'ترجمة مع الرموز التعبيرية';

      this.slide2Title = 'الترجمة الآلية';
      this.slide2Description = 
      'يأخذ Tmoji المكان الذي أنت فيه ولغتك لترجمة مئات الكلمات على الفور مع الرموز التعبيرية';

      this.slide3Title = 'اللغات';
      this.slide3Description = 
      'Tmoji لديها 10 أكثر اللغات شعبية في جميع أنحاء العالم';
 
      this.slide4Title = 'الاقسام';
      this.slide4Description = 
      'Tmoji لديها فئات مثل: الطعام والسفر والأماكن وحالات الطوارئ والطبيعة';

      this.lastTitle = "انه من السهل" ;
      this.lastDescription = 'يود فريق Tmoji أن تكون لديك رحلة جيدة';
      this.lastButtonLabel = 'البدء في استخدام Tmoji';

    } else {

      this.skipLabel = 'Skip';
      this.slide1Title = 'Hello welcome to Tmoji';
      this.slide1Description = 'Translate with emojis';

      this.slide2Title = 'Automatic translation';
      this.slide2Description = 
      'Tmoji take the place where you are and your language ' +
      'to translate hundreds of words instantly with emojis';

      this.slide3Title = 'Languages';
      this.slide3Description = 
      'Tmoji has the 10 most popular languages around the world';
 
      this.slide4Title = 'Categories';
      this.slide4Description = 
      'Tmoji has categories like: ' +
      'food, travel and places, emergencies and nature';

      this.lastTitle = "It's that easy!" ;
      this.lastDescription = 'The Tmoji team wish you ' +
                             'have a good trip!';
      this.lastButtonLabel = 'Start using Tmoji';

    }

    this.slides = [
      {
        title: this.slide1Title,
        description: this.slide1Description,
        image: 'assets/imgs/icon_08.png',
      },
      {
        title: this.slide2Title,
        description: this.slide2Description,
        image: 'assets/imgs/emojis/map.svg',
      },
      {
        title: this.slide3Title,
        description: this.slide3Description,
        image: 'assets/imgs/world.png',
      },
      {
        title: this.slide4Title,
        description: this.slide4Description,
        image: 'assets/imgs/categories.png',
      },
    ];

  }

  ionViewDidEnter() {
    // the root left menu should be disabled on the tutorial page
    this.menu.enable(false);
    this.setTranslation();
  }

  ionViewWillLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }

  onSlideChangeStart(slider) {
    this.showSkip = !slider.isEnd();
    if(slider.isEnd()){
       this.isLastSlide = true
    }
  }

  ionViewWillEnter(){
   
  }


}
