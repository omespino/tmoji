import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WelcomeTutorialPage } from './welcome-tutorial';

@NgModule({
  declarations: [
    WelcomeTutorialPage,
  ],
  imports: [
    IonicPageModule.forChild(WelcomeTutorialPage),
  ],
})
export class WelcomeTutorialPageModule {}
