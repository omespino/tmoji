import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { EmojiDetailsPage } from '../emoji-details/emoji-details';
import emojis from '../../data/emojis';

import { LanguageOptionsPage } from '../language-options/language-options'

@Component({
  selector: 'page-health',
  templateUrl: 'health.html',
})
export class HealthPage {

  galleryType = 'regular';
  emojiDetails = EmojiDetailsPage;
  emojiDataBase : any = [];

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController) {
  }

  ngOnInit(){
    for (var emoji of emojis) {
         if(emoji.category.includes('health'))
          this.emojiDataBase.push(emoji);
    }

  }

  swipeEvent(e) {

     if(e.direction == 2){
          this.navCtrl.parent.select(4);
     } else if (e.direction == 4){
          this.navCtrl.parent.select(2);
     }
  }

  goToEmojiDetail(data) {
    this.navCtrl.push(
      EmojiDetailsPage, { emojiDetailsData: data}
    );
  }

    // language options
  showLangOptions(){
   
   const modal = 
      this.modalCtrl.create(
          LanguageOptionsPage, null, {}); 
          // {cssClass:"custom-class"});
      modal.present();
      modal.onDidDismiss((languageDialogClosedData: any) => {

      // if (languageDialogClosedData){
      //     this.lenguageSelectedByUser = languageDialogClosedData;
      //     console.log(this.lenguageSelectedByUser);
      // }
  
    });  

  }

}
