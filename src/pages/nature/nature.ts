import { Component } from '@angular/core';
import { NavController , ModalController, 
         PopoverController} from 'ionic-angular';
import { EmojiDetailsPage } from '../emoji-details/emoji-details';
import emojis from '../../data/emojis';
import { Storage } from '@ionic/storage';

import { LanguageOptionsPage } from '../language-options/language-options'

@Component({
  selector: 'page-nature',
  templateUrl: 'nature.html',
})
export class NaturePage {

  galleryType = 'regular';
  emojiDetails = EmojiDetailsPage;
  emojiDataBase : any = [];

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    private storage: Storage) {
  }

  ngOnInit(){
    for (var emoji of emojis) {
         if(emoji.category.includes('nature'))
          this.emojiDataBase.push(emoji);
    }
  }

  swipeEvent(e) {

     if(e.direction == 2){
          this.navCtrl.parent.select(5);
     } else if (e.direction == 4){
          this.navCtrl.parent.select(3);
     }
  }

  goToEmojiDetail(data) {
    this.navCtrl.push(
      EmojiDetailsPage, { emojiDetailsData: data}
    );
  }

  // language options
  showLangOptions() {
    const modal =
      this.popoverCtrl.create(
        LanguageOptionsPage, {},
        { cssClass: "custom-popover" });
    modal.present();
    modal.onDidDismiss((langObject: any) => {
    
      if (!langObject) { return };
      if (langObject.action == 'save'){
        this.storage.set('localLangSelectedLabel', langObject.localLang);
        this.storage.set('userLangSelectedLabel', langObject.userLang);
        this.storage.set('localLangSelected', langObject.localLangCode);
        this.storage.set('userLangSelected', langObject.userLangCode);
      }

    });
  }

}