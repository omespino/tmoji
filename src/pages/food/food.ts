import { Component } from '@angular/core';
import { NavController , ModalController, 
         PopoverController } from 'ionic-angular';
import { EmojiDetailsPage } from '../emoji-details/emoji-details';
import emojis from '../../data/emojis';
import { Storage } from '@ionic/storage';

import { LanguageOptionsPage } from '../language-options/language-options'

@Component({
  selector: 'page-food',
  templateUrl: 'food.html',
})

export class FoodPage {

  dinamicPlaceHolder: string = "Search"
  galleryType = 'regular';
  emojiDetails = EmojiDetailsPage;
  emojiDataBase : any = [];

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    private storage: Storage) {

    storage.get('userLangSelected').then((userLangSelected) => {
        this.dinamicPlaceHolder = this.getSearchLabelByCountryCode(userLangSelected);
    });

  }
  
  ngOnInit(){
    for (var emoji of emojis) {
         if(emoji.category.includes('food'))
          this.emojiDataBase.push(emoji);
    }

  }
  
  swipeEvent(e) {

     if(e.direction == 2){
          this.navCtrl.parent.select(2);
     } else if (e.direction == 4){
          this.navCtrl.parent.select(0);
     }
  }


  goToEmojiDetail(data) {
    this.navCtrl.push(
      EmojiDetailsPage, { emojiDetailsData: data}
    );
  }

  // language options
  showLangOptions() {
    const modal =
      this.popoverCtrl.create(
        LanguageOptionsPage, {},
        { cssClass: "custom-popover" });
    modal.present();
    modal.onDidDismiss((langObject: any) => {
    
      if (!langObject) { return };
      if (langObject.action == 'save'){
        this.storage.set('localLangSelectedLabel', langObject.localLang);
        this.storage.set('userLangSelectedLabel', langObject.userLang);
        this.storage.set('localLangSelected', langObject.localLangCode);
        this.storage.set('userLangSelected', langObject.userLangCode);
      }
      
      // document.getElementById('myInputFood')
      // .getElementsByTagName('input')[0]
      // .placeholder = this.getSearchLabelByCountryCode(langObject.userLangCode);



    });
  }
  getSearchLabelByCountryCode(countryCode: string){
    let placeHolderTranslated;

       if (countryCode == 'en') {
          placeHolderTranslated = ' Search';
       } else if (countryCode == 'es') {
          placeHolderTranslated = ' Buscar';
       } else if (countryCode == 'fr') {
          placeHolderTranslated = ' Recherche';
       } else if (countryCode == 'ja') {
          placeHolderTranslated = ' サーチ';
       } else if (countryCode == 'ru') {
          placeHolderTranslated = ' Поиск';
       } else if (countryCode == 'de') {
          placeHolderTranslated = ' Suche';
       } else if (countryCode == 'hi') {
          placeHolderTranslated = ' खोज';
       } else if (countryCode == 'zh') {
          placeHolderTranslated = ' 搜索';
       } else if (countryCode == 'ar') {
          placeHolderTranslated = ' بحث';
       } else if (countryCode == 'pt') {
          placeHolderTranslated = ' Pesquisa';
       }
       return  placeHolderTranslated;
  }

  onInput(){
    let searchValue = 
        document.getElementById('myInputFood')
              .getElementsByTagName('input')[0].value;
    this.emojiDataBase = [];
    if(searchValue !='') { //&& searchValue.length > 3 ){
        for (var emoji of emojis) {
            if(JSON.stringify(emoji).toLocaleLowerCase()              
                  .replace(/á|à|â|ä|æ|ã|å|ā/g,'a')
                  .replace(/è|é|ê|ë|ē|ė|ę/g,'e')
                  .replace(/î|ï|í|ī|į|ì/g,'i')
                  .replace(/ô|ö|ò|ó|œ|ø|ō|õ/g,'o')
                  .replace(/û|ü|ù|ú|ū/g,'u')
                  .replace('ñ','n')
              .includes(searchValue.toLocaleLowerCase()))
              this.emojiDataBase.push(emoji);
        }
    } else {
        for (var emoji of emojis) {
           if(emoji.popular == 'true')
              this.emojiDataBase.push(emoji);
      }
    }       


    console.log(searchValue);

  }
}
