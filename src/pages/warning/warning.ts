import { Component } from '@angular/core';
import { NavController, ModalController, 
         PopoverController } from 'ionic-angular';
import { EmojiDetailsPage } from '../emoji-details/emoji-details';
import emojis from '../../data/emojis';
import { Storage } from '@ionic/storage';

import { LanguageOptionsPage } from '../language-options/language-options'


@Component({
  selector: 'page-warning',
  templateUrl: 'warning.html',
})
export class WarningPage {

  galleryType = 'regular';
  emojiDetails = EmojiDetailsPage;
  emojiDataBase : any = [];

  ngOnInit(){
    for (var emoji of emojis) {
         if(emoji.category.includes('warning'))
          this.emojiDataBase.push(emoji);
    }

  }

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public popoverCtrl: PopoverController,
    private storage: Storage) {
  }

  swipeEvent(e) {

     if(e.direction == 2){
          this.navCtrl.parent.select(4);
     } else if (e.direction == 4){
          this.navCtrl.parent.select(2);
     }
  }

  goToEmojiDetail(data) {
    this.navCtrl.push(
      EmojiDetailsPage, { emojiDetailsData: data}
    );
  }

  // language options
  showLangOptions() {
    const modal =
      this.popoverCtrl.create(
        LanguageOptionsPage, {},
        { cssClass: "custom-popover" });
    modal.present();
    modal.onDidDismiss((langObject: any) => {
    
      if (!langObject) { return };
      if (langObject.action == 'save'){
        this.storage.set('localLangSelectedLabel', langObject.localLang);
        this.storage.set('userLangSelectedLabel', langObject.userLang);
        this.storage.set('localLangSelected', langObject.localLangCode);
        this.storage.set('userLangSelected', langObject.userLangCode);
      }

    });
  }
}
