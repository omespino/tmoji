import { Component } from '@angular/core';
import {
  NavController, AlertController, // AlertController, 
  PopoverController, //ModalController,LoadingController
} from 'ionic-angular';
import { EmojiDetailsPage } from '../emoji-details/emoji-details';
import emojis from '../../data/emojis';
import country_codes from '../../data/country_codes';
import country_srvs from '../../data/country_srvs';
import { Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { LanguageOptionsPage } from '../language-options/language-options'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  dinamicPlaceHolder: string = "Search"
  galleryType = 'regular';
  emojiDetails = EmojiDetailsPage;
  emojiDataBase: any = [];
  emojiRecentDataBase: any = [];
  lenguageSelectedByUser: any = [];
  autoCountryCode: string;

  constructor(
    public navCtrl: NavController,
    // private modalCtrl: ModalController,
    // private loadingCtrl : LoadingController
    private alertCtrl: AlertController,
    private popoverCtrl: PopoverController,
    private events : Events,
    private storage: Storage
  ) {

    //  fetch(this.getRandomIndex(country_srvs))
    //  .then(r => console.log(r))
    //  .catch(e => console.log(e))
    storage.get('userLangSelected').then((userLangSelected) => {
      this.dinamicPlaceHolder = this.getSearchLabelByCountryCode(userLangSelected);
    });

    this.getCountryCodeByIp();

  }

  // when component
  ngOnInit() {
    for (var emoji of emojis) {
      //  if (emoji.popular == 'true')
        this.emojiDataBase.push(emoji);
    }
  }

  ionViewWillEnter() {

  }

  getCountryCodeByIp(){
      fetch('http://ip2c.org/s')
         .then((response) => {
               response.text()
                 .then(text => {
                   this.autoCountryCode = text.split(';')[1]
                   console.log('Fetched CountryCode: ', this.autoCountryCode);
                    // this.alertCtrl.create({
                    //       title: 'Codigo obtenido',
                    //       message: this.autoCountryCode,
                    //       buttons: ['ok']
                    // }).present();
                 } 
               );
         }).catch(err => {
               console.log('req err: ');
               console.log(err.message);    
               this.noAutoTranslateAlert();        
       })
  }
  
  // swipe between pages 
  swipeEvent(e) {
    if (e.direction == 2) {
      this.navCtrl.parent.select(1);
    } else if (e.direction == 4) {
      this.navCtrl.parent.select(0);
    }
  }


  getLangByCountryCode(CODE: string) {

    let lang = "";

    for (var country of country_codes) {
      if (country.code.includes(CODE))
        lang = country.lang;
    }

    return lang;
  }


  // emoji detail page
  goToEmojiDetail(data) {

    this.navCtrl.push(
      EmojiDetailsPage, {
        emojiDetailsData: data,
        langSelectedByUser: this.lenguageSelectedByUser
      }
    );


  }

  // language options
  showLangOptions() {

    if(this.autoCountryCode){
      console.log('dd'+this.autoCountryCode);
    }
    const modal =
      this.popoverCtrl.create(
        LanguageOptionsPage, {
          autoCountryCode: this.autoCountryCode
        },
        { cssClass: "custom-popover" });
    modal.present();
    modal.onDidDismiss((langObject: any) => {
    
      if (!langObject) { return };
      if (langObject.action == 'save'){
        this.storage.set('localLangSelectedLabel', langObject.localLang);
        this.storage.set('userLangSelectedLabel', langObject.userLang);
        this.storage.set('localLangSelected', langObject.localLangCode);
        this.storage.set('userLangSelected', langObject.userLangCode);
      
        // document.getElementById('myInput')
        //     .getElementsByTagName('input')[0]
        //     .placeholder = this.getSearchLabelByCountryCode(langObject.userLangCode);

      }

    });
  }

  getRandomIndex(array: any) {
    return array[Math.floor(Math.random() * array.length)]
  }

  getSearchLabelByCountryCode(countryCode: string){
    let placeHolderTranslated;

       if (countryCode == 'en') {
          placeHolderTranslated = ' Search';
       } else if (countryCode == 'es') {
          placeHolderTranslated = ' Buscar';
       } else if (countryCode == 'fr') {
          placeHolderTranslated = ' Recherche';
       } else if (countryCode == 'ja') {
          placeHolderTranslated = ' サーチ';
       } else if (countryCode == 'ru') {
          placeHolderTranslated = ' Поиск';
       } else if (countryCode == 'de') {
          placeHolderTranslated = ' Suche';
       } else if (countryCode == 'hi') {
          placeHolderTranslated = ' खोज';
       } else if (countryCode == 'zh') {
          placeHolderTranslated = ' 搜索';
       } else if (countryCode == 'ar') {
          placeHolderTranslated = ' بحث';
       } else if (countryCode == 'pt') {
          placeHolderTranslated = ' Pesquisa';
       }
       return  placeHolderTranslated;
  }

  onInput(){
    let searchValue = 
        document.getElementById('myInput')
              .getElementsByTagName('input')[0].value;
    this.emojiDataBase = [];
    if(searchValue !='') { //&& searchValue.length > 3 ){
        for (var emoji of emojis) {
            if(JSON.stringify(emoji).toLocaleLowerCase()              
                  .replace(/á|à|â|ä|æ|ã|å|ā/g,'a')
                  .replace(/è|é|ê|ë|ē|ė|ę/g,'e')
                  .replace(/î|ï|í|ī|į|ì/g,'i')
                  .replace(/ô|ö|ò|ó|œ|ø|ō|õ/g,'o')
                  .replace(/û|ü|ù|ú|ū/g,'u')
                  .replace('ñ','n')
              .includes(searchValue.toLocaleLowerCase()))
              this.emojiDataBase.push(emoji);
        }
    } else {
        for (var emoji of emojis) {
           if(emoji.popular == 'true')
              this.emojiDataBase.push(emoji);
      }
    }       

    console.log(searchValue);

  }

  noAutoTranslateAlert(){
    
    // let titleTranslated = 'Error';
    // let messageTranslated = 'Cannot translate automatically, please select manual translation';
    

    // this.alertCtrl.create({
    //   title: titleTranslated,
    //   message: messageTranslated,
    //   buttons: ['ok']
    // }).present();   
  }
}
