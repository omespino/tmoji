import { Component } from '@angular/core';

import { HomePage } from '../home/home';
import { FoodPage } from '../food/food';
import { TravelPage } from '../travel/travel';
// import { HealthPage } from '../health/health';
import { WarningPage } from '../warning/warning';
import { NaturePage } from '../nature/nature';
// import { AnimalsPage } from '../animals/animals';
import { AboutPage } from '../about/about';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = FoodPage;
  tab3Root = TravelPage;
  tab4Root = WarningPage;
  tab5Root = NaturePage;
  tab6Root = AboutPage;

  constructor() {

  }

}
