import { Component } from '@angular/core';
import { ViewController , Toggle} from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-language-options',
  templateUrl: 'language-options.html',
})

export class LanguageOptionsPage {

	isAutoTranslated: boolean = false
  userLanguage: string = '';
  languageAvailables : any = [];
  locationLanguage: string;
  deviceLanguage: any = window.navigator.language.split('-')[0];

	userLanguageSelectedByUser : string;
  locationLanguageSelectedByUser : string;
  
  // this.storage.clear();
  // place holders
  userPlaceHolder: string;// = "English"
  locationPlaceHolder: string;// =  "Español"

  constructor(
    private viewCtrl: ViewController,
    private storage: Storage
  ) {

  	this.languageAvailables = [

	    'English (english)',         
	    'Español (spanish)',
	    'Français (french)',
	    'Pусский (russian)',
	    '日本語 (japanese)',  
	    '中文 (chinese)',  
	    'हिंदू भाषा (hindi)',  
	    'Deutsch (german)', 
	    'Português (portuguese)',
	    'اللغة العربية (arabic)'
  ]

     // manual mode
     if(this.deviceLanguage == 'en' ) {
      this.locationLanguage = 'Español (spanish)'
    } else {
      this.locationLanguage = 'English (english)'
    }

  // this.storage.get('automaticTranslation').then((automaticTranslation) => {

  //   if(automaticTranslation == true) {
  //     this.isAutoTranslated = true
  //     this.storage.clear();
  //   }

  // });

    if(this.isAutoTranslated) {
      return
    } else {
    
      storage.get('userLangSelected').then((userLangSelected) => {
        if(userLangSelected == undefined) return
          this.deviceLanguage = userLangSelected;
          console.log("userLangSelected",userLangSelected);
        
      });
      
      storage.get('localLangSelectedLabel').then((localLangSelected) => {
        if(localLangSelected == undefined) return
        console.log('localLangSelectedLabel', localLangSelected);
        this.locationLanguage = localLangSelected;
      });

      storage.get('userLangSelectedLabel').then((userLangSelectedLabel) => {
        if(userLangSelectedLabel == undefined) return
        this.userLanguage = userLangSelectedLabel;
        console.log("userLangSelectedLabel",userLangSelectedLabel);
      });
    }

  	if (this.deviceLanguage == 'en' ){
      this.userLanguage = this.languageAvailables[0];
  	} else if (this.deviceLanguage == 'es'){
  		this.userLanguage = this.languageAvailables[1];
  	} else if (this.deviceLanguage == 'fr'){
  		this.userLanguage = this.languageAvailables[2];
  	} else if (this.deviceLanguage == 'ru'){
  		this.userLanguage = this.languageAvailables[3];
  	} else if (this.deviceLanguage == 'ja'){
  		this.userLanguage = this.languageAvailables[4];
  	} else if (this.deviceLanguage == 'zh'){
  		this.userLanguage = this.languageAvailables[5];
  	} else if (this.deviceLanguage == 'hi'){
  		this.userLanguage = this.languageAvailables[6];
  	} else if (this.deviceLanguage == 'de'){
  		this.userLanguage = this.languageAvailables[7];
  	} else if (this.deviceLanguage == 'pt'){
  		this.userLanguage = this.languageAvailables[8];
  	} else if (this.deviceLanguage == 'ar'){
  		this.userLanguage = this.languageAvailables[9];
  	} else {
      this.userLanguage = this.languageAvailables[0];
    }
    this.userPlaceHolder = this.userLanguage

    if(this.deviceLanguage == 'en' ) {
      this.locationPlaceHolder = 'Español'
    } else {
      this.locationPlaceHolder = 'English'
    }
  }

  onAction(action: string){

    // location language
    if (this.locationLanguage == this.languageAvailables[0]){
      this.locationLanguageSelectedByUser =  'en';
    } else if (this.locationLanguage == this.languageAvailables[1]){
      this.locationLanguageSelectedByUser =  'es';
    } else if (this.locationLanguage == this.languageAvailables[2]){
      this.locationLanguageSelectedByUser =  'fr';
    } else if (this.locationLanguage == this.languageAvailables[3]){
      this.locationLanguageSelectedByUser =  'ru';
    } else if (this.locationLanguage == this.languageAvailables[4]){
      this.locationLanguageSelectedByUser =  'ja';
    } else if (this.locationLanguage == this.languageAvailables[5]){
      this.locationLanguageSelectedByUser =  'zh';
    } else if (this.locationLanguage == this.languageAvailables[6]){
      this.locationLanguageSelectedByUser =  'hi';
    } else if (this.locationLanguage == this.languageAvailables[7]){
      this.locationLanguageSelectedByUser =  'de';
    } else if (this.locationLanguage == this.languageAvailables[8]){
      this.locationLanguageSelectedByUser =  'pt';
    } else if (this.locationLanguage == this.languageAvailables[9]){
      this.locationLanguageSelectedByUser =  'ar';
    } else {
      this.locationLanguageSelectedByUser =  'en';
    }

    // user language
    if (this.userLanguage == this.languageAvailables[0]){
      this.userLanguageSelectedByUser =  'en';
    } else if (this.userLanguage == this.languageAvailables[1]){
      this.userLanguageSelectedByUser =  'es';
    } else if (this.userLanguage == this.languageAvailables[2]){
      this.userLanguageSelectedByUser =  'fr';
    } else if (this.userLanguage == this.languageAvailables[3]){
      this.userLanguageSelectedByUser =  'ru';
    } else if (this.userLanguage == this.languageAvailables[4]){
      this.userLanguageSelectedByUser =  'ja';
    } else if (this.userLanguage == this.languageAvailables[5]){
      this.userLanguageSelectedByUser =  'zh';
    } else if (this.userLanguage == this.languageAvailables[6]){
      this.userLanguageSelectedByUser =  'hi';
    } else if (this.userLanguage == this.languageAvailables[7]){
      this.userLanguageSelectedByUser =  'de';
    } else if (this.userLanguage == this.languageAvailables[8]){
      this.userLanguageSelectedByUser =  'pt';
    } else if (this.userLanguage == this.languageAvailables[9]){
      this.userLanguageSelectedByUser =  'ar';
		}	 else {
      this.userLanguageSelectedByUser =  'en';
    }	
		
    this.viewCtrl.dismiss({ 
                action:action, 
                userLang:this.userLanguage,
								localLang:this.locationLanguage,
								userLangCode:this.userLanguageSelectedByUser,
								localLangCode:this.locationLanguageSelectedByUser
    });
    
  }

  ngOnInit(){
    // let emojiDetail = this.navParams.get('emojiDetailsData');
		// console.log(this.deviceLanguage);
		// console.log(this.isAutoTranslated);

	}
	
	onToggle(toggle: Toggle){
		if(toggle.checked) {
      this.isAutoTranslated = true;
      // this.storage.set('automaticTranslation',true);
      // console.log("save as checked")
		} else {
      this.isAutoTranslated = false;
      // this.storage.set('automaticTranslation',false);
      // console.log("save as unchecked")

		}
  }
}