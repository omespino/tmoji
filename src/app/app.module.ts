import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { TabsPage } from '../pages/tabs/tabs';
import { EmojiDetailsPage } from '../pages/emoji-details/emoji-details';

// services
import { LanguageService } from '../services/language';

// tabbar items 
import { HomePage } from '../pages/home/home';
import { FoodPage } from '../pages/food/food';
import { TravelPage } from '../pages/travel/travel';
import { HealthPage } from '../pages/health/health';
import { WarningPage } from '../pages/warning/warning';
import { NaturePage } from '../pages/nature/nature';
import { AboutPage } from '../pages/about/about';
import { LanguageOptionsPage } from '../pages/language-options/language-options';
import { WelcomeTutorialPage } from '../pages/welcome-tutorial/welcome-tutorial';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Libraries added by omespino
import { SocialSharing } from '@ionic-native/social-sharing';
import { HttpClientModule } from '@angular/common/http'; 
import { AdMobFree } from '@ionic-native/admob-free';
import { IonicStorageModule } from '@ionic/storage';
import { AdsenseModule } from 'ng2-adsense';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    LanguageOptionsPage,
    AboutPage,

    FoodPage,
    TravelPage,
    HealthPage,
    WarningPage,
    NaturePage,
    EmojiDetailsPage,
    WelcomeTutorialPage,
  ],
  imports: [
  BrowserModule,
  HttpClientModule,
  IonicModule.forRoot(
    MyApp,{ 
        // hide tabs in sub pages
        tabsHideOnSubPages: true,
      }
  ),
    IonicStorageModule.forRoot(),
    AdsenseModule.forRoot({
      adClient: 'ca-pub-7207379778635847', //replace with your client from google adsense
      adSlot: 6653856071 //replace with your slot from google adsense
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FoodPage,
    TravelPage,
    HealthPage,
    WarningPage,
    NaturePage,
    EmojiDetailsPage,
    AboutPage,
    TabsPage,
    LanguageOptionsPage,
    WelcomeTutorialPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler},
    LanguageService,
    SocialSharing,
    AdMobFree,
    AdsenseModule
  ]
})
export class AppModule {}
