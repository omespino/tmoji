import { Component, ViewChild } from '@angular/core';
import { Platform , AlertController, Nav} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { AdMobFree, AdMobFreeBannerConfig } from '../../node_modules/@ionic-native/admob-free';
import { WelcomeTutorialPage } from '../pages/welcome-tutorial/welcome-tutorial';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {

  rootPage:any = TabsPage;
  @ViewChild(Nav) nav: Nav;
  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen, 
    admobFree: AdMobFree,
    storage: Storage
    ) {
      platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      splashScreen.hide();
      this.showBannerAd(admobFree);

      if (platform.is('ios')) {
        statusBar.styleDefault();
      } else {
        statusBar.backgroundColorByHexString('#ffffff');
      }

      storage.get('showSlide').then((showSlide) => {

          // console.log(showSlide);
          if(showSlide == undefined) showSlide = true;
          if(showSlide == true) {
            this.nav.setRoot(WelcomeTutorialPage, {}, {
              animate: true,
              direction: 'back',
              animation: 'ios-transition'

            });
          }

      });

    });
  }

  async showBannerAd(admobFree:AdMobFree){
    const bannerConfig: AdMobFreeBannerConfig = {
      // add your config here
      // for the sake of this example we will just use the test config
      isTesting: false,
      id: 'ca-app-pub-7207379778635847/4332489656', // with the slash '/' not the '~'
      // bannerAtTop:true,
      autoShow: true
     };
     admobFree.banner.config(bannerConfig); 
     admobFree.banner.prepare()
       .then(() => {
        admobFree.banner.show();
         // banner Ad is ready
         // if we set autoShow to false, then we will need to call the show method here
       })
       .catch(e => console.log(e));
  }
}
